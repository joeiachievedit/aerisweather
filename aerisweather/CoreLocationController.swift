//
//  CoreLocationController.swift
//  weatheralerts
//
//  Copyright (c) 2014 iAchieved.it LLC. All Rights Reserved.

import Foundation
import CoreLocation

class CoreLocationController : NSObject, CLLocationManagerDelegate {
  
  var locationManager:CLLocationManager = CLLocationManager()
  
  override init() {
    super.init()
    self.locationManager.delegate = self
    self.locationManager.requestAlwaysAuthorization()

  }
  
  func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
    println("didChangeAuthorizationStatus")
    
    switch status {
    case .NotDetermined:
      println(".NotDetermined")
      break
      
    case .Authorized:
      println(".Authorized, start monitoring for significant location changes")
      self.locationManager.startMonitoringSignificantLocationChanges()
      break
      
    case .Denied:
      println(".Denied")
      break
      
    default:
      println("Unhandled authorization status")
      break
    
    }
  }
  
  func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
    
    let location = locations.last as CLLocation
    
    println("didUpdateLocations:  \(location.coordinate.latitude), \(location.coordinate.longitude)")
    
    let geocoder = CLGeocoder()
    geocoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, e) -> Void in
      if let error = e {
        println("Error:  \(e.localizedDescription)")
      } else {
        let placemark = placemarks.last as CLPlacemark
        
        let userInfo = [
          "city":     placemark.locality,
          "state":    placemark.administrativeArea,
          "country":  placemark.country,
          "timestamp":location.timestamp
        ] as NSDictionary
        
        let url = NSFileManager.defaultManager().containerURLForSecurityApplicationGroupIdentifier("group.aerisweather")
        let file = url!.URLByAppendingPathComponent("aerisweather.plist")
        
        if userInfo.writeToURL(file, atomically: true) {
          println("Saved last location:  \(userInfo)")
        }
        
        // Update our UI if it is visible
        NSNotificationCenter.defaultCenter().postNotificationName("LOCATION_AVAILABLE", object: nil, userInfo: userInfo)

      }
    })

    
    
  }
  
}